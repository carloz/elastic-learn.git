package com.elastic.eslearn._1_index;

import com.carrotsearch.hppc.cursors.ObjectObjectCursor;
import com.elastic.eslearn._0_client.EsUtils;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.admin.indices.exists.types.TypesExistsResponse;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsResponse;
import org.elasticsearch.action.admin.indices.settings.get.GetSettingsResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.settings.Settings;

/**
 * 通过 indicesAdminClient 管理 索引
 */
public class IndexAdmin {

	public static void main(String[] args) throws Exception {
		TransportClient client = EsUtils.getSingleClient();
		IndicesAdminClient indicesAdminClient = client.admin().indices();
		// 判断索引是否存在
		IndicesExistsResponse indicesExistsResponse = indicesAdminClient.prepareExists("books").get();
		System.out.println("is index exists? " + indicesExistsResponse.isExists());
		// 判断type是否存在
		TypesExistsResponse typesExistsResponse = indicesAdminClient.prepareTypesExists("books")
				.setTypes("_doc", "article")
				.get();
		System.out.println("is type exists? " + typesExistsResponse.isExists());
		// 创建一个索引, 不能重复创建
		String indexName = "java_api_index";
//		CreateIndexResponse createIndexResponse = indicesAdminClient.prepareCreate("java_api_index").get(); // 索引名必须小写
//		System.out.println("create index: " + createIndexResponse.isAcknowledged());
		// 创建索引并设置Settings
		CreateIndexResponse createIndexResponse = indicesAdminClient.prepareCreate(indexName)
				.setSettings(Settings.builder().put("index.number_of_shards", 3).put("index.number_of_replicas", 2)
				).get();
		// 更新副本
		AcknowledgedResponse updateAck = indicesAdminClient
				.prepareUpdateSettings(indexName)
				.setSettings(Settings.builder().put("index.number_of_replicas", 0))
				.get();
		System.out.println("update index success? " + updateAck.isAcknowledged());
		// 获取Settings
		GetSettingsResponse getSettingsResponse = indicesAdminClient.prepareGetSettings(indexName, "books").get();
		for (ObjectObjectCursor<String, Settings> cursor : getSettingsResponse.getIndexToSettings()) {
			String index = cursor.key;
			Settings settings = cursor.value;
			Integer shards = settings.getAsInt("index.number_of_shards", null);
			Integer replicas = settings.getAsInt("index.number_of_replicas", null);
			System.out.println("index = " + index + ", shards = " + shards + ", replicas = " + replicas);
		}

		// 使用Java API 设置mapping
//		CreateIndexResponse createIndexResponse = indicesAdminClient.prepareCreate("twitter")
//				.addMapping("_doc", XContentFactory.jsonBuilder()
//						.startObject()
//						.startObject("properties")
//						.startObject("name")
//						.field("type", "keyword")
//						.endObject().endObject().endObject()
//						).get();
		// 获取mapping
		GetMappingsResponse mappingsResponse = indicesAdminClient.prepareGetMappings("twitter").get();
		ImmutableOpenMap<String, MappingMetaData> mappings = mappingsResponse.getMappings().get("twitter");
		MappingMetaData mappingMetaData = mappings.get("_doc");
		System.out.println(mappingMetaData.getSourceAsMap());

		// 删除索引
		AcknowledgedResponse deleteAck = indicesAdminClient.prepareDelete(indexName).get();
		System.out.println("deleteAck.isAcknowledged() = " + deleteAck.isAcknowledged());

		// 刷新
		indexName = "twitter";
		String typeName = "_doc";
		indicesAdminClient.prepareRefresh().get();
		indicesAdminClient.prepareRefresh(indexName).get();
//		indicesAdminClient.prepareRefresh(indexName, indexName1).get();

		// 关闭索引
		AcknowledgedResponse closeAck = indicesAdminClient.prepareClose(indexName).get();
		System.out.println("closeAck.isAcknowledged() = " + closeAck.isAcknowledged());

		// 打开索引
		AcknowledgedResponse openAck = indicesAdminClient.prepareOpen(indexName).get();
		System.out.println("openAck.isAcknowledged() = "+ openAck.isAcknowledged());

		// 设置别名
		AcknowledgedResponse aliasAck = indicesAdminClient.prepareAliases().addAlias(indexName, "twitter_alias").get();
		System.out.println("aliasAck.isAcknowledged() = " + aliasAck.isAcknowledged());

		// 获取别名
		GetAliasesResponse aliasesResponse = indicesAdminClient.prepareGetAliases("twitter_alias").get();
		System.out.println(aliasesResponse.toString());

	}
}
