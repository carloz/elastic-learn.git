package com.learn.elasticsearchnews.utils;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.net.UnknownHostException;

/**
 * 避免每次都创建对象，使用单例模式来创建 TransportClient
 */
public class EsUtils {

	private volatile static RestHighLevelClient client;
	public static RestHighLevelClient getClient() throws UnknownHostException {
		if (null == client) {
			synchronized (RestHighLevelClient.class) {
				String CLUSTER_NAME = "elasticsearch";  // 访问 http://10.10.139.42:9200/ 获取
				String HOST_IP = "10.10.139.42";
				int TCP_PORT = 9200;
				String PROTOCOL = "http";
				client = new RestHighLevelClient(
						RestClient.builder(new HttpHost(HOST_IP, TCP_PORT, PROTOCOL))
				);
			}
		}
		return client;
	}
}
