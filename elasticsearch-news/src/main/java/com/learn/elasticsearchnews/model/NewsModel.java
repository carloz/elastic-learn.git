package com.learn.elasticsearchnews.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * 文件对象
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NewsModel {
	private Integer id;
	private String newsType;
	private String richText;
	private String createTime;
}
