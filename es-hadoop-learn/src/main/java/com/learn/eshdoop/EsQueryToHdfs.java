package com.learn.eshdoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.elasticsearch.hadoop.mr.EsInputFormat;

import java.io.IOException;

/**
 * 查询 Elasticsearch 数据 然后将结果写入 HDFS
 */
public class EsQueryToHdfs {

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		conf.set("es.nodes", "10.10.139.42");
		conf.set("es.port", "9200");
		conf.set("es.resource", "blog/_doc");
		conf.set("es.output.json", "true");
		conf.set("es.query", "?q=title:python");
		Job job = Job.getInstance(conf, "query es to HDFS");
		job.setMapperClass(EsToHdfs.MyMapper.class);
		job.setNumReduceTasks(1);
		job.setMapOutputKeyClass(NullWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setInputFormatClass(EsInputFormat.class);
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[0]));
		job.waitForCompletion(true);
	}

	public static class MyMapper extends Mapper<Writable, Writable, NullWritable, Text> {
		@Override
		protected void map(Writable key, Writable value, Context context) throws IOException, InterruptedException {
			Text text = new Text();
			text.set(value.toString());
			context.write(NullWritable.get(), text);
		}
	}
}
