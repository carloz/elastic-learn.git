# Elasticsearch搜索新浪实时新闻demo



## 目录

1. 《ELK》系统搭建完成
2. 从新浪财经直播 采集数据，存入mysql
3. 使用logstash-jdbc插件 从mysql 同步到 Elasticsearch
4. 使用spring-boot搭建工程，搜索新闻，并展示



### 1、《ELK》系统搭建

参考我的上篇文章



### 2、从新浪财经直播 采集数据，存入mysql

采集地址：<http://finance.sina.com.cn/7x24/>

采集脚本：crawl_living_sina_news.py

代码仓库：elasticsearch-news/doc



### 3、使用logstash-jdbc插件 从mysql 同步到 Elasticsearch

1. 在 Elasticsearch中建立 sina_news 索引；
2. 使用logstash-input-jdbc从mysql中 把 sina_news库下的 news 表中的数据同步到 Elasticsearch；



### 4、从Elasticsearch中搜索数据，返回给前端

1. pom.xml引入依赖

```xml
<dependency>
	<groupId>org.elasticsearch</groupId>
	<artifactId>elasticsearch</artifactId>
	<version>7.0.0</version>
</dependency>
<dependency>
	<groupId>org.elasticsearch.client</groupId>
	<artifactId>elasticsearch-rest-client</artifactId>
	<version>7.0.0</version>
</dependency>
<dependency>
	<groupId>org.elasticsearch.client</groupId>
	<artifactId>elasticsearch-rest-high-level-client</artifactId>
	<version>7.0.0</version>
</dependency>

```

2. 建立搜索入口，低用搜索方法

```java
/**
 * @param keywords
 * @return 搜索结果页面
 */
@RequestMapping("/search")
public ModelAndView searchFile(String keywords, @Nullable Integer from, @Nullable Integer size) {
	ArrayList<NewsModel> hitsList = null;
	try {
		hitsList = mySearchService.searchSinaNews(keywords, from, size);
	} catch (Exception e) {
		e.printStackTrace();
	}
	log.info(keywords + "：共搜到：" + hitsList.size() + " 条数据！");
	ModelAndView mv = new ModelAndView("result.html");
	mv.addObject("keywords", keywords);
	mv.addObject("resultList", hitsList);
	return mv;
}
```

3. 使用REST API 调用 Elasticsearch 查询数据

```java
@Service
public class MySearchService {
	public ArrayList<NewsModel> searchSinaNews(String keywords, Integer from, Integer size) throws Exception {
		if (null == from) from = 0;
		if (null == size) size = 10;
		RestHighLevelClient client = EsUtils.getClient();
		...
		...
		...
		SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
		ArrayList<NewsModel> resultList = new ArrayList<>();
		if(searchResponse.status() == RestStatus.OK) {
			SearchHits searchHits = searchResponse.getHits();
			for (SearchHit hit : searchHits) {
				Map<String, Object> resMap =hit.getSourceAsMap();
				Map<String, NewsModel> newsMap = new HashMap<>();
				NewsModel news = new NewsModel(
						Integer.valueOf(resMap.get("id").toString()),
						resMap.get("news_type").toString(),
						resMap.get("rich_text").toString(),
						resMap.get("create_time").toString());
				resultList.add(news);
			}
		}
		return resultList;
	}
}
```

4. 在浏览器中访问展示









