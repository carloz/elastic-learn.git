# 使用es-hadoop在Elasticsearch和HDFS之间做数据移动



### 1、从 HDFS 到 Elasticsearch

准备json文档，上传到HDFS中

vi blog.json

```json
{"id":5,"title":"JavaScript高级程序设计","language":"javascript","author":"NicholasC.Zakas","price":66.4,"publish_time":"2012-03-02","desc":"JavaScript技术经典名著。"}
{"id":3,"title":"Python科学计算","language":"python","author":"张若愚","price":81.4,"publish_time":"2014-01-02","desc":"零基础学python，光盘中坐着度假开发winPython运行环境，涵盖了Python各个扩展库。"}
{"id":4,"title":"Python基础教程","language":"python","author":"Helant","price":54.5,"publish_time":"2014-03-02","desc":"经典的python入门教程，层次鲜明，结构严谨，内容详实。"}
```

[root@localhost hadoop]# hadoop fs -put blog.json /work

编写代码，从 hdfs 读取数据，写入到 Elasticsearch;

pom中指定Map任务的主类：com.learn.eshdoop.HdfsToEs；

运行：mvn clean package 生成对应的jar 包

[root@localhost hadoop]# mv es-hadoop-learn-1.0-SNAPSHOT-jar-with-dependencies.jar HdfsToEs.jar

执行Job任务，读取HDFS数据到Elasticsearch：

[root@localhost hadoop]# hadoop jar HdfsToEs.jar /work/blog.json

在Kibana 中 查看

GET blog/_mapping

GET blog/_search



### 2、从Elasticsearch 到 HDFS

##### 2.1、将索引 blog 保存到 hdfs

添加Java类 EsToHdfs ， 从Elasticsearch中查询索引数据，存放到HDFS;

修改pom.xml中的主类为：com.learn.eshdoop.EsToHdfs;

mvn clean package 重新打包

上传以后执行：

[root@localhost hadoop]# mv es-hadoop-learn-1.0-SNAPSHOT-jar-with-dependencies.jar EsIndexToHdfs.jar

执行Job任务：

[root@localhost hadoop]# hadoop jar EsIndexToHdfs.jar /work/blog_mapping

读取数据：

[root@localhost hadoop]# hadoop fs -cat /work/blog_mapping/part-r-00000



##### 2.2、将带条件查询 blog 的 数据 保存到 hdfs

添加Java类EsQueryToHdfs，从Elasticsearch中查询数据，然后写入 HDFS;

在pom.xml中修改主类名称为：com.learn.eshdoop.EsQueryToHdfs；

mvn clean package 重新打包，然后上传；

[root@localhost hadoop]# mv es-hadoop-learn-1.0-SNAPSHOT-jar-with-dependencies.jar EsQueryToHdfs.jar

[root@localhost hadoop]# hadoop jar EsQueryToHdfs.jar /work/EsQueryToHdfs

[root@localhost hadoop]# hadoop fs -cat /work/EsQueryToHdfs/part-r-00000



