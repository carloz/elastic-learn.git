package com.elastic.eslearn._4_aggs;

import com.elastic.eslearn._0_client.EsUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.Max;
import org.elasticsearch.search.aggregations.metrics.MaxAggregationBuilder;

/**
 * 聚合分析， MaxAggregationDemo
 */
public class MaxAggregationDemo {
	public static void main(String[] args) throws Exception {
		MaxAggregationBuilder maxAggregationBuilder = AggregationBuilders.max("agg")
				.field("price");
		SearchResponse searchResponse = EsUtils.getSingleClient().prepareSearch("books")
				.addAggregation(maxAggregationBuilder)
				.get();
		Max maxAgg = searchResponse.getAggregations().get("agg");
		double value = maxAgg.getValue();
		System.out.println(value);
	}
}
