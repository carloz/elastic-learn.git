package com.elastic.eslearn._2_doc;

import com.elastic.eslearn._0_client.EsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 生成json格式文档的方法：
 * 1. 把 json 格式 手工转化为 byte[] 或 String；
 * 2. 使用Map，代表一个JSON结构；
 * 3. 使用内置帮助类 XContentFactory 的 jsonBuilder() 方法；
 * 4. 使用 Jackson 等第三方库 把JavaBean 转化为 json 序列；
 */
public class CreateDoc {

	public String indexName = "twitter";
	public String typeName = "_doc";

	public static void main(String[] args) throws Exception {

		TransportClient client = EsUtils.getSingleClient();
		CreateDoc createDoc = new CreateDoc();
//		createDoc.createByJsonStr(client);
//		createDoc.createByMap(client);
//		createDoc.createByXContentType(client);
		createDoc.createByJackson(client);
	}

	/**
	 * 1. 把 json 格式 手工转化为 byte[] 或 String；
	 * @param client
	 */
	public void createByJsonStr(TransportClient client) {
		String json = "{" +
				"\"user\":\"kimchy\"," +
				"\"postDate\":\"2013-01-30\"," +
				"\"message\":\"trying out Elasticsearch\"" +
				"}";
		IndexResponse response = client.prepareIndex(indexName, typeName, "1")
				.setSource(json, XContentType.JSON)
				.get();
		System.out.println(response.status());
	}

	/**
	 * 2. 使用Map，代表一个JSON结构；
	 * @param client
	 */
	public void createByMap(TransportClient client) {
		Map<String, Object> doc = new HashMap<String, Object>();
		doc.put("user", "kimchy");
		doc.put("postDate", "2013-01-30");
		doc.put("message", "trying out Elasticsearch");
		IndexResponse response = client.prepareIndex(indexName, typeName, "2")
				.setSource(doc)
				.get();
		System.out.println(response.status());
	}

	/**
	 * 3. 使用内置帮助类 XContentFactory 的 jsonBuilder() 方法；
	 * @param client
	 */
	public void createByXContentType(TransportClient client) throws IOException {
		XContentBuilder doc = XContentFactory.jsonBuilder()
				.startObject()
				.field("user", "kimchy")
				.field("postDate", "2013-01-30")
				.field("message", "trying out Elasticsearch")
				.endObject();
		System.out.println(doc.toString());
		IndexResponse response = client.prepareIndex(indexName, typeName, "3")
				.setSource(doc)
				.get();
		System.out.println(response.status());
	}

	/**
	 * 4. 使用 Jackson 等第三方库 把JavaBean 转化为 json 序列；
	 * @param client
	 * @throws JsonProcessingException
	 */
	public void createByJackson(TransportClient client) throws JsonProcessingException {
		User user = new User("Kaylee", new Date(), "trying out Elasticsearch");
		ObjectMapper mapper = new ObjectMapper();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		mapper.setDateFormat(format);
		byte[] doc = mapper.writeValueAsBytes(user);
		IndexResponse response = client.prepareIndex(indexName, typeName, "5")
				.setSource(doc, XContentType.JSON)
				.execute()
				.actionGet();
		System.out.println(response.status());
	}

	@Getter
	@Setter
	@AllArgsConstructor
	@NoArgsConstructor
	class User{
		private String user;
		private Date postDate;
		private String message;
	}

}
