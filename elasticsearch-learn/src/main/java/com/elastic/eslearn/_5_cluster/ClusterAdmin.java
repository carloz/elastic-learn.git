package com.elastic.eslearn._5_cluster;

import com.elastic.eslearn._0_client.EsUtils;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.cluster.health.ClusterHealthStatus;
import org.elasticsearch.cluster.health.ClusterIndexHealth;

/**
 * 集群管理
 */
public class ClusterAdmin {
	public static void main(String[] args) throws Exception {
		ClusterHealthResponse healthResponse = EsUtils.getSingleClient()
				.admin().cluster().prepareHealth().get();
		String clusterName = healthResponse.getClusterName();
		int numberOfDataNodes = healthResponse.getNumberOfDataNodes();
		int numberOfNodes = healthResponse.getNumberOfNodes();
		System.out.println(clusterName + ", numberOfDataNodes = " + numberOfDataNodes
				+ ", numberOfNodes = " + numberOfNodes);
		for (ClusterIndexHealth health : healthResponse.getIndices().values()) {
			String index = health.getIndex();
			int numberOfShards = health.getNumberOfShards();
			int numberOfReplicas = health.getNumberOfReplicas();
			ClusterHealthStatus status = health.getStatus();
			System.out.println(index.getBytes().toString() + ", numberOfShards = " + numberOfShards
					+ ", numberOfReplicas = " + numberOfReplicas + ", status + " + status);
		}
	}
}
