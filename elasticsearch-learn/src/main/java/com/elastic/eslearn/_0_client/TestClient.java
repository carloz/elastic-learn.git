package com.elastic.eslearn._0_client;

import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * elasticsearch 7.0.0
 * TransportClient 接口已经快被废弃了
 */
public class TestClient {
	public static String CLUSTER_NAME = "elasticsearch";  // 访问 http://10.10.139.42:9200/ 获取
	public static String HOST_IP = "10.10.139.42";
	public static int TCP_PORT = 9300;  // 注意是9300
	public static void main(String[] args) throws UnknownHostException {
		Settings settings = Settings.builder()
				.put("cluster.name", CLUSTER_NAME)
				.build();
		TransportClient client = new PreBuiltTransportClient(settings)
				.addTransportAddress(new TransportAddress(InetAddress.getByName(HOST_IP), TCP_PORT));
		GetResponse getResponse = client.prepareGet("books", "_doc", "1").get();
		System.out.println(getResponse.getSourceAsString());
	}
}
