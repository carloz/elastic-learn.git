

# 新浪新闻采集脚本

### 1、使用python 2.7 解析器

配置环境变量，path下添加：
D:\python\tools\Python27;D:\python\tools\Python27\Scripts;

安装依赖模块：
pip install requests
https://sourceforge.net/projects/mysql-python/
下载64位版本的mysql: http://www.codegood.com/download/11/



### 2、新建数据库

CREATE DATABASE `sina_news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `news_type` enum('其他','央行','观点','市场','数据','公司','行业','宏观','A股') NOT NULL,
  `create_time` datetime DEFAULT NULL,
  `rich_text` text,
  PRIMARY KEY (`id`)
);



### 3、运行，即可在数据库里看到数据




