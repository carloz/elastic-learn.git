package com.learn.elasticsearchnews;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElasticsearchNewsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElasticsearchNewsApplication.class, args);
	}

}
